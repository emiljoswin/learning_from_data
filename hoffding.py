from math import log

def calculate_N (bound, eps, M):
  '''
    Return the vlaue of N according to Hoeffding inequality
  '''

  numerator = log(bound/ float((2 * M))) 
  denominator = -2 * (eps)**2

  ans = numerator/denominator
  return ans
