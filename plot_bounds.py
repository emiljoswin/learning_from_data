
'''
  Plotting generalization bounds for very large Ns
  using 
  a) vc_bound
  b) Rademachaer Penalty Bound
  c) Parrondo and Van den Broek
  d) Devroye

  NOTE: I forgot one important thing while submitting the ans.
  The ans was right for the wrong reason. If N < dvc we have a 
  straight formula for the growth function ie, 2^N.
  Instead I went alongwith the same growth function that I used 
  for very large values of N.
  However, for very small N (even before reaching 6), devroy bound quickly get past the 
  Parrondo van de Broek bound.
'''


from pylab import *
from math import log, sqrt
from decimal import *

ion()

def test_plot ():
  x = [10, 20, 30]
  y = [log(10)/10, log(20)/2000, log(30)/3]
  plt.plot(x, y, 'g-')
  plt.show()

dvc = 50
delta = 0.05
#growth function = N^dvc

def find_eps (N, growth):
  '''
    Function to find the valus of eps for pv inequality.
    We set eps0 = 0.1 and try to get eps1, then eps3 until
    epsn-1 ~= epsn
  '''
  eps = 0.1 # this is arbitrarily chosen. eps may even go beyond it
            # and still satisfy the inequality

  while True:
    temp = sqrt( 1/N * (2*eps + log( (6*growth)) - log(delta )) )
    if temp == eps:
      return temp
    eps = temp
 
def find_eps2 (N, growth):
  '''
    Function to find the valus of eps for pv inequality.
    We set eps0 = 0.1 and try to get eps1, then eps3 until
    epsn-1 ~= epsn
  '''
  eps = 0.1 # this is arbitrarily chosen. eps may even go beyond it
            # and still satisfy the inequality
  

  while True:
    #temp = sqrt(1/(2*N) * (4*eps + 4*eps*eps) + log(4*(growth)/Decimal(delta)))
    #using properties of logarithm to modify the above equation

    temp = sqrt(1/(2*N) * ((4*eps + 4*eps*eps) + log(4) + log(growth) - log(delta) ))

    if temp == eps:
      return temp
    eps = temp

def vc_bound (N):
  '''
    The vapnik-chervonenki bound
  '''
  #N = float(N)
  growth = (2*int(N))**dvc

  if N < dvc:
    growth = 2**N

  ans = sqrt( (8/N) * (log( (4*growth) ) - log (delta )) )
  return ans

def rp_bound (N):
  '''
    The Rademacher Penalty Bound
  '''
  growth = (int(N))**dvc

  if N < dvc:
    growth = 2**N

  ans = sqrt((2 * (log(2*N) + log(growth))) / N) + sqrt(2/N * log (1/delta)) + 1/N
  return ans

def pv_bound (N):
  '''
    The Parrondo and Van den Broek inequality.
    The inequality contains esp on both sides.So,
    try to find the maximum eps that satisfy the inequality
    for large values of N.
  '''

  #growth = (2*10000)**dvc
  #eps = find_eps(10000, growth) # to optimize, this can be done outside,
                            # or eps should be found for each N ?
  growth = (2*int(N))**dvc

  if N < dvc:
    growth = 2**N

  eps = find_eps(N, growth) # to optimize, this can be done outside,

  ans = sqrt( 1/N * (2*eps + log( (6*growth)) - log(delta )) )
  return ans

def d_bound (N):
  '''
    The Devroye inequality.
    The inequality contains esp on both sides.So,
    try to find the maximum eps that satisfy the inequality
    for large values of N.
  '''
  growth = (int(N)*int(N))**dvc # int is required for preventing overflow

  if N < dvc:
    growth = 2**N

  eps = find_eps2(N,growth)

  #growth = (int(10000)*int(10000))**dvc # int is required for preventing overflow
  #eps = find_eps2(10000,growth)

  ans = sqrt(1/(2*N) * ((4*eps + 4*eps*eps) + log(4) + log(growth) - log(delta) ) )

  return ans

def start ():
  x = []
  y_vc = []
  y_rp = []
  y_pv = []
  y_d = []
  #for i in range(10000, 20000, 1):
  for i in range(5, 50, 1):
  #for i in range(10000, 10000000, 1000):
    x.append(i)
    y_vc.append(vc_bound(float(i)))
    y_rp.append(rp_bound(float(i)))
    y_pv.append(pv_bound(float(i)))
    y_d.append(d_bound(float(i)))

    
  print y_rp, y_vc, y_pv
  vc, = plt.plot(x, y_vc, 'g-', label='vc')
  rp, = plt.plot(x, y_rp, 'r-', label='rp')
  pv, = plt.plot(x, y_pv, 'b-', label='pv')
  d, = plt.plot(x, y_d, 'y-', label='d')

  #draw()
  #plt.legend(handles=[blah, desp])
  plt.legend()
  #plt.show()

