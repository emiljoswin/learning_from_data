'''
  Apply SVM classification to a real world data-set. The character
  recognition problem from the US postal service.
  I was not able to install libsvm, so, I am using the svm module that
  comes along with sklearn whose implementation is based on libsvm.
'''

from sklearn.svm import SVC
import numpy as np


def get_p_vs_all (p, Dtrain):
  '''
    Returns the data set p vs all from the input
  '''

  X_train, Y_train = [], []
  for i in range(len(Dtrain)):
    d = Dtrain[i]
    X_train.append([d[1], d[2]])
    if d[0] == p:
      Y_train.append(1)
    else:
      Y_train.append(-1)
      
  return X_train, Y_train


def get_p_vs_q (p, q, Dtrain):
  '''
    Returns the data set p vs q for the input
  '''

  X_train, Y_train = [], []

  for i in range(len(Dtrain)):
    d = Dtrain[i]
    if d[0] == p:
      #X_train = numpy.append(X_train, [d[1], d[2]])
      #Y_train = numpy.append(Y_train, [1.0])
      X_train.append([d[1], d[2]])
      Y_train.append(1.0)
    elif d[0] == q:
      #X_train = numpy.append(X_train, [d[1], d[2]])
      #Y_train = numpy.append(Y_train, [-1.0])
      X_train.append([d[1], d[2]])
      Y_train.append(-1.0)
  
  return X_train, Y_train

def solve_and_find_Ein_and_n_sv (X, Y, C, Q, Xt=None, Yt=None):
  '''
    Solve the SVM and find the in sample error
    And return the number of support vectors
    The library returns the number of SVs as an array of numbers
    where each number corresponds to the no of SV of a particular class
    [#svs of class1, #svs of class2, #svs of class3..]
    The total number of svs = sum(this array)

    Xt and Yt are the test points to calculate eout
  '''

  X = np.array(X)
  Y = np.array(Y)

  kernel = 'poly'
  #C = 0.01
  #Q = 2

  #TODO: I missed the gamma and coeff0 terms and spend around a day 
  #figuring out what went wrong with this problem.

  clf = SVC(C=C, kernel=kernel, degree=Q, gamma=1, coef0 = 1)
  #clf = SVC(C=C, kernel=kernel, degree=Q)
  clf.fit(X, Y)

  # In-sample error
  e_in = 0

  #from sklearn.metrics import accuracy_score

  #y_pred = clf.predict(X)
  #accuracy = accuracy_score(Y, y_pred)
  #print accuracy, 1-accuracy


  for i in range(len(X)):
    y = clf.predict(X[i])
    if y[0] != Y[i]:
      e_in += 1

  e_in = e_in/float(len(X))
  
  # calculate eout if provided
  e_out = None
  if Xt != None:
    e_out = 0
    for i in range(len(Xt)):
      y = clf.predict(Xt[i])
      if y[0] != Yt[i]:
        e_out += 1

    e_out = e_out/float(len(Xt))
    
  return e_in, clf.n_support_, e_out

def q2_to_q4 (Dtrain, Dtest):
  '''
    Answers for questions from 2 to 4
  '''

  # for the highest E_in
  C = 0.01 #from question
  Q = 2
  print 'find the highest ein'
  for k in range(0,  9, 2):
    X, Y = get_p_vs_all(k, Dtrain) # means get 0 vs all's dtrain

    e_in, n_svs, e_out = solve_and_find_Ein_and_n_sv(X, Y, C, Q)
    print 'e_in for ',k , e_in, sum(n_svs)

  # for the lowest E_in
  print 'find the lowest ein'
  for k in range(1, 10, 2):
    X, Y = get_p_vs_all(k, Dtrain) # means get 0 vs all's dtrain

    e_in, n_svs, e_out = solve_and_find_Ein_and_n_sv(X, Y, C, Q)
    print 'e_in for ',k , e_in, sum(n_svs)


def q5toq6 (Dtrain, Dtest):
  C = [0.001, 0.01, 0.1, 1.0]
  X, Y = get_p_vs_q(1, 5, Dtrain)
  X_test, Y_test = get_p_vs_q(1, 5, Dtest)

  for c in C:
    Q = 2
    e_in, n_svs, e_out = solve_and_find_Ein_and_n_sv(X, Y, c, Q, X_test, Y_test)
    print 'ein for c=', c, e_in, 'Q = ', Q, 'no of SVs = ', sum(n_svs), n_svs, 'e_out= ', e_out
    Q = 5
    e_in, n_svs, e_out = solve_and_find_Ein_and_n_sv(X, Y, c, Q, X_test, Y_test)
    print 'ein for c=', c, e_in, 'Q = ', Q, 'no of SVs = ', sum(n_svs), n_svs, 'e_out= ', e_out
  


def q7toq8 (Dtrain):
  '''
    Choose C using 10-fold cross validation
  '''

  from sklearn.cross_validation import KFold
  import numpy as np

  C = [0.0001, 0.001, 0.01, 0.1, 1]
  Q = 2
  #X, Y = get_p_vs_q(1, 5, Dtrain)
  ##X, Y = get_p_vs_q(4, 6, Dtrain) #ghost problem

  #X = np.array(X)
  #Y = np.array(Y)

  #kf = KFold(len(Y), 10) # create 10 fold cross validation set

  # Question asks to iterate a 100 times (here 3) and pick the minimum.
  # The minimum is obvious in the very first run here.

  import random

  e_val_min = 1

  for i in range(4):
    random.shuffle(Dtrain)
    print 'rand', Dtrain[i]
    X, Y = get_p_vs_q(1, 5, Dtrain)
    X = np.array(X)
    Y = np.array(Y)
    
    kf = KFold(len(Y), 10)

    e_vals = [0, 0, 0, 0, 0]
    for train, test in kf:
      X_train, Y_train = X[train], Y[train]
      X_test, Y_test = X[test], Y[test]

      for i in range(len(C)):
        e_in, n_svs, e_val = solve_and_find_Ein_and_n_sv(X, Y, C[i], Q, X_test, Y_test)
        e_vals[i] += e_val


    for i in range(len(e_vals)):
      print e_vals[i]/float(len(kf)), C[i]

    #for c in C:
    #  sum_e_val = 0
    #  sum_n_svs = 0
    #  #kf = KFold(len(Y), 10)

    #  for train, test in kf:
    #    X_train, Y_train = X[train], Y[train]
    #    X_test, Y_test = X[test], Y[test]

    #    e_in, n_svs, e_val = solve_and_find_Ein_and_n_sv(X, Y, c, Q, X_test, Y_test)

    #    #print 'e', e_val, e_in
    #    sum_e_val += e_val
    #    sum_n_svs += sum(n_svs)

    #  #print 'eval', sum_e_val/float(len(kf)), 'for c= ', c
    #  #'svs =', sum_n_svs/float(len(kf))
    #  sum_e_val = sum_e_val/float(len(kf))

    #  e_val_min = min(sum_e_val, e_val_min)
    #  print e_val_min

  #print e_val_min
 

def solv_SVM_RBF (X, Y, C, Xt=None, Yt=None):
  '''
  '''

  X = np.array(X)
  Y = np.array(Y)

  kernel='rbf'

  clf = SVC(C=C, kernel=kernel, gamma=1)
  clf.fit(X, Y)

  e_in = 0
  for i in range(len(X)):
    y = clf.predict(X[i])
    if y[0] != Y[i]:
      e_in += 1

  e_in = e_in/float(len(X))
  
  if Xt: #do out of sample calculation if Xt and Yt are provided
    e_out = 0
    for i in range(len(Xt)):
      y = clf.predict(Xt[i])
      if y[0] != Yt[i]:
        e_out += 1

    e_out = e_out/float(len(Xt))

  return e_in, e_out


def q9toq10 (Dtrain, Dtest):
  X, Y = get_p_vs_q(1, 5, Dtrain)
  X_test, Y_test = get_p_vs_q(1, 5, Dtest)
  
  C = [0.01, 1, 100, 10000, 1000000]
  for c in C:
    e_in, e_out = solv_SVM_RBF(X, Y, c, X_test, Y_test)
    print 'c = ', c, 'ein = ', e_in, 'eout =', e_out
  

def start ():
  file_train = 'w8features.train.txt'
  file_test = 'w8features.test.txt'

  Dtrain = [] # the training data set
  with open(file_train) as f:
    for line in f:
      l = line.split()
      Dtrain.append([float(l[0]), float(l[1]), float(l[2]) ])

  Dtest = [] # the test data set
  with open(file_test) as f:
    for line in f:
      l = line.split()
      Dtest.append([float(l[0]), float(l[1]), float(l[2]) ])


  #q2_to_q4(Dtrain, Dtest)

  #q5toq6(Dtrain, Dtest)

  q7toq8(Dtrain)

  #q9toq10(Dtrain, Dtest)

  ##ghost problem
  #X, Y = get_p_vs_q(4, 6, Dtrain)
  #X_test, Y_test = get_p_vs_q(4, 6, Dtest)

  #e_in, n_svs = solve_and_find_Ein_and_n_sv(X, Y, 1, 2, X_test, Y_test)
  #print 'ein for c=', 1, e_in, 'Q = ', 2, 'no of SVs = ', sum(n_svs), n_svs




