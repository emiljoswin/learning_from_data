'''
  error, E(u, v) = (ue^v - 2ve_u)^2.
  eta = 0.1 (learning rate, not step size)

  Q1) minimum iterations required to take error E(v,u) to fall below 10^-14.
'''

from math import e

def do_gd1 (u, v, Eu, Ev, eta):
  '''
    q7. in each iteration, first move along u, then reevalueate
    and move along v
  '''
  E = lambda u, v: (u*e**(v) - 2*v*e**(-u))**2

  k = 0 
  while k <= 15:
    k += 1
    err = E(u,v)
    print(k, 'err', err, u, v)

    step_size_u = eta * Eu(u, v)
    u = u - step_size_u

    step_size_v = eta * Ev(u, v)

    #u = u - step_size_u
    v = v - step_size_v

  err = E(u,v)
  print('after', err, 'u', u, 'v', v)


def do_gd (u, v, Eu, Ev, eta):
  '''
    q4, q5, q6
  '''
  E = lambda u, v: (u*e**(v) - 2*v*e**(-u))**2

  k = 0 
  while k <= 15:
    '''
      E goes below 10**-14 at iteration 10
    '''
    k += 1
    err = E(u,v)
    print(k, 'err', err, u, v)

    step_size_u = eta * Eu(u, v)
    step_size_v = eta * Ev(u, v)

    u = u - step_size_u
    v = v - step_size_v

  err = E(u,v)
  print('after', err, 'u', u, 'v', v)
    


def start ():
  u = 1
  v = 1
  
  Eu = lambda u, v: 2 * (u*e**v - 2*v*e**(-u)) * (e**v + 2*v*e**(-u)) # partial derivative wrt u
  Ev = lambda u, v: 2 * (u*e**v - 2*v*e**(-u)) * (u*e**v - 2*e**(-u)) # partial derivative wrt v 

  eta = 0.1

  do_gd1(u, v, Eu, Ev, eta)
  
