from random import shuffle, uniform, randint

s = ['h', 't']

def flip_coins (N, coins):
  ''' flip N coins simultaneously '''

  min_head = N
  for i in range(N):
    shuffle(s)
    coins[i][s[1]] += 1
    if coins[i][s[1]] <= min_head:
      min_head = coins[i][s[1]]

  return coins, min_head

def initialize_coins (N):
  coins = {}
  for i in range(N):
    coins[i] = {'h': 0, 't': 0}

  return coins

def start ():
  N = 1000 # no of coins
  T = 10 # no of trials
  E = 100000 # no of experiments

  coins = initialize_coins (N)
  nu_1, nu_rand, nu_min = 0, 0, 0

  for i in range(E):
    # one experment
    nu_1_e, nu_rand_e, nu_min_e = 0.0, 0.0, 0.0
    coins = initialize_coins (N)
    for i in range(T):
      coins, min_head = flip_coins(N, coins)

    nu_1_e += coins[0]['h']
    nu_rand_e += coins[randint(0, 99)]['h']
    nu_1_e /= T
    nu_rand_e /= T
    nu_min_e  = (min_head * 1.0)/T
    #print nu_1_e, nu_rand_e, nu_min_e

    nu_1 += nu_1_e
    nu_rand += nu_rand_e
    nu_min += nu_min_e

  nu_1 /= E
  nu_rand /= E
  nu_min /= E


  return nu_1_e, nu_rand_e, nu_min_e, nu_1, nu_rand, nu_min
    
    
    
