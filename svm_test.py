import numpy as np
X = np.array([[-1, 1], [-2, 2], [1, 1], [2, 1]])
Y = np.array([1, 1, 2, 2])

from sklearn.svm import SVC
clf = SVC()
clf.fit(X,Y)
print clf.predict([[-0.8, -1]])
print clf.predict([[-1, 1], [1, 1]])
for i in X:
  print 'pred', i, clf.predict(i)
