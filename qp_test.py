from cvxopt import matrix
from cvxopt import solvers

P = matrix([[1.0, 0.0], [0.0, 0.0]]) # the alpha
print P
q = matrix([3.0, 4.0]) # the (-1)t matrix
G = matrix([[-1.0, 0.0, -1.0, 2.0, 3.0], [0.0, -1.0, -3.0, 5.0, 4.0]]) #identity matrix
h = matrix([0.0, 0.0, -15.0, 100.0, 80.0]) # 0 matrix

sol = solvers.qp(P, q, G, h)

print 'x', sol['x']
print 'status', sol['status']

