'''
  We are given a bunch of non-linear transformations
  numbered from 0 to 7.
  1, x1, x2, x1^2, x2^2, x1x2, |x1-x2|, |x1+x2|
  use data in w6q2in.txt
'''
from pylab import *


def sign (a):
  if a < 0: return -1
  return 1

def get_X (X, k):
  '''
    return the x matrix from the given value of k
  '''
  x = []

  for i in range(len(X)):
    train = []

    x1 = X[i][0]
    x2 = X[i][1]
    train += [1, x1, x2]

    train += [x1**2] #k = 3

    if k > 3: #k = 4
      train += [x2**2]
    if k > 4: #k = 5
      train += [x1*x2]
    if k > 5: #k = 6
      train += [abs(x1-x2)]
    if k > 6: #k = 7
      train += [abs(x1+x2)]

    x.append(train)

  x = matrix(x)
  return x

def obtain_Xk_and_Xkval (Xtrain, Xval, k):
  '''
    Return the Dtrain and Dval matrix for value k
  '''
  Xt = get_X (Xtrain, k)
  Xv = get_X (Xval, k)
  
  return Xt, Xv

def calculate_e_val (X, W, Y):
  '''
    Calculate the classification error on validation set,
  '''
  Y_obtained = X*W
  e_val = 0
  for i in range(len(Y)):
    if sign(Y_obtained[i, 0]) != Y[i, 0]:
      e_val += 1

  #print e_val
  return e_val/float(len(Y))

def get_X_and_Y (filename):
  '''
    Return X and Y in non matrix format by reading from file
  '''
  X, Y = [], []
  with open(filename) as f:
    for line in f:
      l = line.split()
      Y.append(float(l[2]))
      x1 = float(l[0])
      x2 = float(l[1])
      X.append([x1, x2])

  return X, Y


def validation (train_s, train_e, val_s, val_e):
  '''
    Split the data into training points and validation points.
    Training points are from train_s to train_e
    and Validation points are from val_s to val_e
    And test on the non-linear transformations 3 to 7.
  '''

  filename = 'w6q2in.txt'
  X, Y = get_X_and_Y (filename)

  Xtrain = X[train_s:train_e]
  Xval = X[val_s:val_e]

  Y_train = matrix(Y[train_s:train_e]).reshape((train_e - train_s), 1)
  Y_val = matrix(Y[val_s:val_e]).reshape((val_e - val_s), 1)

  X_test, Y_test = get_X_and_Y ('w6q2out.txt')
  Y_test = matrix(Y_test).reshape(len(Y_test), 1)
  
  for k in range(3, 8):
    # k from 3 to 7

    X_train_k, X_val_k = obtain_Xk_and_Xkval (Xtrain, Xval, k)
    X_dagger = inv(transpose(X_train_k) * X_train_k) * transpose(X_train_k)
    W = X_dagger * Y_train

    #print W
    Eval = calculate_e_val (X_val_k, W, Y_val)
    X_test_k, temp = obtain_Xk_and_Xkval(X_test, [], k)
    Eout = calculate_e_val (X_test_k, W, Y_test)
    print 'k= ', k, 'Eval', Eval, 'Eout', Eout


def start ():
  validation(0, 25, 25, 35)
  validation(25, 35, 0, 25)
