'''
  FINAL Q7-Q10
  Using regularized linear regression to perform classification
  on the US Postal Service Zip Code dataset with extracted
  features of symmetry and intensity
'''

from pylab import *

from w6q2 import * # calculate e_in and e_out

def obtain_data_from_file (filename):
  '''
    Read from the filename and return the values as such
  '''
  d_train = [] #training data set
  with open(filename) as f:
    for line in f:
      l = line.split()
      d_train.append([float(l[0]), float(l[1]), float(l[2])])

  return d_train

def obtain_X_and_Y (dtrain, p, transform, q = None):
  '''
    Do a 'p' vs all classification of the dtrain or
    'p' vs 'q' classification if 'q' is provided.
    If transform = true, do non-linear transformation.
  '''
  x, y = [], []
  for i in dtrain:
    if not q: # p vs all classification
      if not transform:
        x.append([1, i[1], i[2]])
      else:
        x.append([1, i[1], i[2], i[1]*i[2], i[1]**2, i[2]**2])

      if i[0] == p:
        y.append([1])
      else:
        y.append([-1])

    else: # p vs q classification
      if i[0] == p:
        y.append([1])
        if not transform:
          x.append([1, i[1], i[2]])
        else:
          x.append([1, i[1], i[2], i[1]*i[2], i[1]**2, i[2]**2])
      elif i[0] == q:
        y.append([-1])
        if not transform:
          x.append([1, i[1], i[2]])
        else:
          x.append([1, i[1], i[2], i[1]*i[2], i[1]**2, i[2]**2])

    #if not transform:
    #  x.append([1, i[1], i[2]])
    #else:
    #  x.append([1, i[1], i[2], i[1]*i[2], i[1]**2, i[2]**2])

  return x, y
      

def do_linear_regression (x, y, lam):
  '''
    Do linear regressio with lambda = lam
    inputs are matrices, output is also a matrix
  '''
  lami = identity(x.shape[1]) * lam
  xdagger = inv(transpose(x) * x + lami) * transpose(x)
  w = xdagger * y
  return w

  
def start ():
  
  file_test = 'final_features.test.txt'
  file_train = 'final_features.train.txt'

  d_train = obtain_data_from_file(file_train)
  d_test = obtain_data_from_file(file_test)


  print 'Q:7'
  transform = False #feature transformation flag

  p = 5 # the 'one' in one vs all classification
  lam = 1 # lambda = 1
  for p in range(5, 10):

    xtrain, ytrain = obtain_X_and_Y(d_train, p, transform)
    xtrain = matrix(xtrain)
    ytrain = matrix(ytrain)

    w = do_linear_regression(xtrain, ytrain, lam)
    #print w
    e_in = calculate_e_in(w, xtrain, ytrain)
    print 'p =', p, 'Ein', e_in
    #print xtrain, xtrain.shape, ytrain, ytrain.shape


  print 'Q:8'

  transform = True

  for p in range(0, 5):

    xtrain, ytrain = obtain_X_and_Y(d_train, p, transform)
    xtrain = matrix(xtrain)
    ytrain = matrix(ytrain)

    w = do_linear_regression(xtrain, ytrain, lam)
    #print w
    e_in = calculate_e_in(w, xtrain, ytrain)

    xtest, ytest = obtain_X_and_Y(d_test, p, transform)
    xtest = matrix(xtest)
    ytest = matrix(ytest)
    e_out = calculate_e_out(w, xtest, ytest)

    print 'p =', p, 'Ein', e_in, 'Eout', e_out
    #print xtrain, xtrain.shape, ytrain, ytrain.shape

  print 'Q:10'
  p, q = 1, 5  
  lams = [1, 0.01]
  transform = True

  for i in range(len(lams)):
    xtrain, ytrain = obtain_X_and_Y(d_train, p, transform, q)
    xtrain = matrix(xtrain)
    ytrain = matrix(ytrain)

    w = do_linear_regression(xtrain, ytrain, lams[i])
    e_in = calculate_e_in(w, xtrain, ytrain)

    
    xtest, ytest = obtain_X_and_Y(d_test, p, transform, q)
    xtest = matrix(xtest)
    ytest = matrix(ytest)
    e_out = calculate_e_out(w, xtest, ytest)

    print 'lamda=', lams[i], 'Ein', e_in, 'Eout', e_out

  print 'Q:9'
  lam = 1
  for p in range(0, 10):
    xtrain, ytrain = obtain_X_and_Y(d_train,p, False)
    xtrain = matrix(xtrain)
    ytrain = matrix(ytrain)
    w = do_linear_regression(xtrain, ytrain, lam)
    xtest, ytest = obtain_X_and_Y(d_test, p, False)
    xtest = matrix(xtest)
    ytest = matrix(ytest)
    e_out = calculate_e_out(w, xtest, ytest)

    xtrain, ytrain = obtain_X_and_Y(d_train,p, True)
    xtrain = matrix(xtrain)
    ytrain = matrix(ytrain)
    w = do_linear_regression(xtrain, ytrain, lam)
    xtest, ytest = obtain_X_and_Y(d_test, p, True)
    xtest = matrix(xtest)
    ytest = matrix(ytest)
    e_out_trans = calculate_e_out(w, xtest, ytest)

    improvement = (e_out - e_out_trans) / e_out_trans
    print 'p=', p, 'Eout without', e_out, 'Eout with trans', e_out_trans, 'improvement =', improvement
    
    
  
