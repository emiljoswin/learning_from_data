'''
  training set file w6q2in.txt
  test set file w6q2out.txt
  Running linear regression with non-linear transformation

  phi(x1, x2) = (1, x1, x2, x1^2, x2^2, x1x2, |x1-x2|, |x1+x2|)
'''

from pylab import *
 
def sign (a):
  if a < 0: return -1
  return 1


def calculate_e_in (w, X, Y):
  y_obtained = X*w
  #print y_obtained, Y
  e_in = 0
  for i in range(len(Y)):
    if sign(y_obtained[i, 0]) != Y[i, 0]:
      e_in += 1

  return e_in/float(len(Y))

def calculate_e_out (w, X, Y):
  '''
    Calculate y from w and X and compare with Y and return e_out
  '''
  y = X*w
  e_out = 0
  for i in range(len(Y)):
    if sign(y[i, 0]) != Y[i, 0]:
      e_out += 1

  return e_out/float(len(Y))
    
def obtain_X_and_Y (filename):
  '''
    obtain the test points from filename and return the X
    and Y in matrix form
  '''
  
  X, Y = [], []
  with open(filename) as f:
    for line in f:
      l = line.split()
      Y.append(float(l[2]))
      x1 = float(l[0])
      x2 = float(l[1])
      X.append([1, x1, x2, x1**2, x2**2, x1*x2, abs(x1 - x2), abs(x1+x2)])

  X = matrix(X)
  Y = matrix(Y).reshape(len(Y), 1)
  return X, Y

def linear_regression (weight_decay):
  '''
    do linear regression and calculate e_out and e_in
  '''
  filein = 'w6q2in.txt'
  X, Y = obtain_X_and_Y(filein)

  fileout = 'w6q2out.txt'
  X_test, Y_test = obtain_X_and_Y(fileout)

  if weight_decay:
    #k = [-3, 3, 2, 1, 0, -1, 2]
    k = range(-10, 10) # for question number 6
    for i in range(len(k)):
      lam = 10**k[i] #lambda
      lami = lam*identity(X.shape[1])
      X_dagger = inv(transpose(X)*X + lami) * transpose(X)
      w = X_dagger * Y
      e_in = calculate_e_in(w, X, Y)

      e_out = calculate_e_out(w, X_test, Y_test)
      print 'weight decay with k=', k[i], 'Ein', e_in, 'Eout', e_out

  else:

    X_dagger = inv(transpose(X) * X) * transpose(X)
    w = X_dagger * Y
    e_in = calculate_e_in(w, X, Y)

    e_out = calculate_e_out(w, X_test, Y_test)

    print 'Eout', e_in, 'Ein', e_out

def start ():
  
  #linear_regression(None) # without weight decay
  linear_regression(True)

  #filein = 'w6q2in.txt'
  #X, Y = obtain_X_and_Y(filein)

  #X_dagger = inv(transpose(X) * X) * transpose(X)
  #w = X_dagger * Y

  #e_in = calculate_e_in(w, X, Y)
  #fileout = 'w6q2out.txt'
  #X_test, Y_test = obtain_X_and_Y(fileout)

  #e_out = calculate_e_out(w, X_test, Y_test)

  #print 'Eout', e_in, 'Ein', e_out


