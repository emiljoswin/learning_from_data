'''
  The first program I wrote was not converging because I was not including
  the bias ie, adding an extra w0x0. You need to make sure that 
  while updating the weight the bias term also needs to be updated.
  Else, you would classify them with a line that contains some kind
  of an intercept eg,

  |   \ <---- the original line from the trainig set
  | -  \ +\ + + +
  | - -  \ +\ + +
  | - - -  \ +\ +
  | - - - -  \ +\ <--- the perceptron line
  |_________________

  X = (0, x1, x2) <--- no bias/bias wont get added because x0 = 0
  X = (1, x1, x2) <--- with bias
'''
import matplotlib.pyplot as plt
import time
from pylab import * # for ion
from random import uniform, shuffle

ion()

def get_random_line (l1, l2):
  x = [uniform(l1, l2), uniform(l1, l2)]
  y = [uniform(l1, l2), uniform(l1, l2)]
  return (x, y) 

def get_rand (n, l1, l2):
  # return n random numbers between l1 and l2
  x = []
  for i in range (n):
    x.append(uniform(l1, l2))
  return x

def get_line_eq (linex, liney):
  # return (slope, yintercept) y = mx + c
  m = (liney[1] - liney[0])/(linex[1] - linex[0]) #(y1-y2)/(x1-x2)
  c = (liney[0] - m*linex[0]) # c = y - mx
  return (m, c)

def plot_decision_boundary (w1, w2, w3, l1, l2, color='g-'):
  '''
    The line equation is obtained in canonical form Ax + By + C = 0
    or A + Bx + Cx = 0 depending on the way you initialize the training
    set, where A, B, C correspond to w1, w2,w3
    Obtain y from the above equation
    Therefore y = -(w1/w3 - w2/w3 * x)
  '''
  plt.plot([l1, l2], [-w1/w3 - w2/w3*l1, -w1/w3 - w2/w3*l2], color)
  draw()
  
def extend_line(m, c, l1, l2, color='y-'):
  '''
    This is not how we plot the decision boundary/hypothesis
  '''
  x = [l1, l2]
  y = [m*l1 + c, m*l2 + c]
  plt.plot(x, y, color)
  draw()

def is_left (point, m, c):
  x = (point[1] - c)/m
  if x > point[0]:
    return True
  return False
 
def dot_product (a, b):
  if len(a) != len(b):
    print 'DOT PRODUCT NOT POSSIBLE'
    return False
  ans = 0
  for i in range(len(a)):
    ans += a[i] * b[i]
  return ans

def sign(a):
  if a < 0: return -1
  return 1

def plearn (training_set, h):
  # implementing perceptron learning algorithm
  #h = [0.0, 0.0, 0.0]
  l1, l2 = -1, 1
  #h = [uniform(l1, l1), uniform(l1, l2), uniform(l1, l2)]
  iterations = 0
  while True:
    iterations += 1

    miss_points = []
    for i in range(len(training_set)):
      y = sign(dot_product(h, training_set[i][0]))
      if y != training_set[i][1]:
        miss_points.append(training_set[i])
        
    if len(miss_points) == 0:
      #print 'converged...'
      break

    shuffle(miss_points)
    random_point = miss_points.pop()
    #update h
    h[0] += random_point[0][0] * random_point[1]
    h[1] += random_point[0][1] * random_point[1]
    h[2] += random_point[0][2] * random_point[1]

  return ("iter", iterations, h)

def test_hyp_and_line(h, m, c):
  points = 1000
  x = get_rand(points, -1, 1)
  y = get_rand(points, -1, 1)
  disagree_cnt = 0.0

  for i in range(points):
    if is_left((x[i], y[i]), m, c):
      if sign(dot_product(h, (1, x[i], y[i]))) != -1:
        disagree_cnt += 1
      #print '-1', sign(dot_product(h, (1, x[i], y[i])))
    else:
      if sign(dot_product(h, (1, x[i], y[i]))) != 1:
        disagree_cnt += 1
      #print '1', sign(dot_product(h, (1, x[i], y[i])))

  print("prob", disagree_cnt/points)
      
  
def start (N):
  #N is the total number of points
  l1, l2 = -1, 1
  x = get_rand(N, l1, l2)
  y = get_rand(N, l1, l2)
  (linex, liney) = get_random_line(l1, l2)

  #linex, liney = [-1, 0, 1], [-1, 0, 1] # test line m = 1 and c = 0

  plt.plot(x, y, 'ro')
  plt.plot(linex, liney, 'g-', lw=2)
  plt.axis([-1, 1, -1, 1])
  ##plt.plot([-1, 0, 0.5], [-0.5, 0.7, 1], 'y-', lw = 2)

  (m, c) = get_line_eq(linex, liney)
  extend_line(m, c, l1, l2)

  training_set = []

  for i in range(len(x)):
    if is_left([x[i], y[i]], m, c):
      training_set.append(((1, x[i], y[i]), -1))
    else:
      training_set.append(((1, x[i], y[i]), 1))
      
  h = [0.0, 0.0, 0.0]
  (a, iterations, h) = plearn(training_set, h)
  #test_hyp_and_line(h, m, c)
  extend_line(h[1], h[2], l1, l2)
  return (a, iterations, h)

def run ():
  N = 1000
  points = 100
  k = 0
  h = [0, 0, 0]
  for i in range(N):
    y = start(points)
    #print y[1]
    h[0] += y[2][0]
    h[1] += y[2][1]
    h[2] += y[2][2]
    k += y[1]

  h[0] /= N
  h[1] /= N
  h[2] /= N

  print(k/N, h)
  

      
