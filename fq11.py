
'''
  Problems 11 and 12. I did not find the answer to Q 12.
'''
from sklearn.svm import SVC
import numpy as np

from pylab import *

def do_transform (X, Y):
  
  z1 = lambda x1, x2: x2**2 - 2*x1 - 1
  z2 = lambda x1, x2: x1**2 - 2*x2 + 1

  zx, zy = [], []
  for i in range(len(X)):
    zx.append([ z1(X[i][0], X[i][1]), z2(X[i][0], X[i][1]) ])
    zy.append(Y[i])

  return zx, zy


def plot (Z, Y):
  z1, z2 = [], []

  for i in range(len(Z)):
    z1.append(Z[i][0])
    z2.append(Z[i][1])
    if Y[i] == 1:
      plt.plot(Z[i][0], Z[i][1], 'yo')
    elif Y[i] == -1:
      plt.plot(Z[i][0], Z[i][1], 'bo')
    
  
def do_svm (x, y):
  '''
    Am just trying out to see if doing it via SVM works
  '''
  X = np.array(x)
  Y = np.array(y)
  kernel = 'poly'
  Q = 2
  coeff0 = 1
  C = 1e2
  clf = SVC(C=C, kernel=kernel, degree=Q, gamma=1, coef0 = 1)

  clf.fit(X, Y)
  e_in = 0
  for i in range(len(X)):
    y_ob = clf.predict(X[i])
    if y[i] != Y[i]:
      e_in += 0

  print clf.n_support_, e_in/float(len(Y))



def start ():
  x_train = [[1, 0], [0, 1], [0, -1], [-1, 0], [0, 2], [0, -2], [-2, 0]]
  y_train = [-1, -1, -1, 1, 1, 1, 1]

  z_train, z_label = do_transform(x_train, y_train)
  #plot(z_train, z_label)
  plot(x_train, y_train)
  do_svm(x_train, y_train)
