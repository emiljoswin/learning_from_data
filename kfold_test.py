import numpy as np
from sklearn.cross_validation import KFold, LeavePOut

def start ():
  X = np.array([[0., 0.], [1., 1.], [-1., -1.], [2., 2.], [3., 3.], [4., 4.], [5., 5.], [6., 6.], [7., 7.], [8., 8.], [9., 9.]])

  Y = np.array([0, 1, 0, 1, 0, 1, 0, 1, 2, 3, 4])

  #kf = KFold(len(Y), 2, indices=False)
  kf = KFold(len(Y), 5)

  print X
  for train, test in kf:
    print train, test
    print X[train]

  #print ''
  #lpo = LeavePOut(len(Y), len(Y)/5)

  #for train, test in lpo:
  #  print train, test
