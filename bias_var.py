'''
  HW: Week 4
  Target functionn f:[-1,1] -> R and f(x) = sin(pi*x).
  Training set has only two examples picked independently
  and the learning algorithm picks hypothesis that 
  minimizes the mean square error on the examples.

  Ans:
  objective: min [(sin(pi*x1) - a*x1)^2 + (sin(pi*x2) - a*x2)^2]/2
  for values of a,

  therefore, 
  a = (sin(pi*x1) + sin(pi*x2))/(x1 + x2)


'''

from pylab import *
import math, random

ion()

def sample_plot ():
  '''
    plots a sample sine function
  '''
  l1, l2 = -1, 1
  x, y = [], []
  plt.plot([-1, 1], [0, 0], 'k-')
  plt.plot([0, 0], [-1, 1], 'k-')
  for i in linspace(l1, l2, 100):
    x.append(i)
    y.append(math.sin(i * math.pi)) #sin(x) return sine of x radians

  plt.plot(x, y, 'c-')
  plt.show()


def plot_line (f,  x1, x2):
  ''' plots the line ax
      f is the hypothesis function
  '''

  x, y = [], []

  plt.plot([x1, x2], [math.sin(math.pi*x1), math.sin(math.pi*x2)], 'ro')
  plt.show()

  #x.append(x1)
  #y.append(f(x1))
  #x.append(x2)
  #y.append(f(x2))

  for i in linspace(-1, 1, 20): #because nonlinear functions need
                                #points in order to plot as a curve

  #for i in range(10):
    #x1 = random.uniform(-1, 1)
    x1 = i
    y1 = f(x1)
    x.append(x1)
    y.append(y1)
    
  plt.plot(x, y, 'b-')
  plt.show()


def do_linear_regression (x1, x2):
  '''
    this method works if we do not prepend 1 to x.
    TODO
    How is that possible?
    Ans: The hypothesis is ax and not ax + b.
  '''

  #w = [1, 1] # w[1] is a
  l1, l2 = -1, 1
  #x0 = 1
  #X = matrix([[x0, x1], [x0, x2]]) # This is plain wrong. Realised hard way


  #X = matrix([[x1], [x2]]) # hypothesis ax

  #X = matrix([[1], [1]]) #hypothesis b

  #X = matrix([[1, x1], [1, x2]]) # hypothesis ax + b, two variables

  #X = matrix([[x1**2], [x2**2]]) # hypothesis ax^2

  X = matrix([[1, x1**2], [1, x2**2]]) # hypothesis ax^2 + b, two variables

  Y = matrix([math.sin(x1*math.pi), math.sin(x2*math.pi)]).reshape(2,1)
  X_dagger = inv(transpose(X) * X) * transpose(X)
  W = X_dagger * Y
  return W
  
  
def mean_sqrt_err (x1, x2):
  '''
    This is one of the methods that is found to work. I got it from
    https://github.com/santiaago/caltech.ml/blob/master/hw4.py
    I am not really sure why this method works.
  '''
  y1 = math.sin(x1*math.pi)
  y2 = math.sin(x2*math.pi)

  dy = abs(y2-y1)/2.
  dx = abs(x2-x1)/2.
  return dx, dy


def compute_bias (g_bar, f, g):
  '''
    Compute the bias between g-average and target function
  '''
  exp = 1000
  l1, l2 = -1, 1
  avg_bias = 0
  for i in range(exp):
    x1 = random.uniform(l1, l2)
    fx = f(x1)
    #fx = math.sin(math.pi*x1)
    #gx = g_bar * x1
    gx = g(x1)
    avg_bias += (gx - fx)**2

  bias = avg_bias/exp
  return bias


def compute_variance (g_bar, f, foo):
  '''
    Compute the variance E[(gd(X) - gbar(X))^2]
    NOTE: I am not really sure if this is the right method.
    I got the right answer though. Time is runnint out fast. :(

    Here, foo(x) is gd(x) and f(x) is gbar(x)
  '''

  exp = 1000
  l1, l2 = -1, 1

  var = lambda a, b: (a - b)**2
  avg_var = 0
  avg_ga, avg_gb = 0, 0

  for i in range(exp):
    x1 = random.uniform(l1, l2)
    x2 = random.uniform(l1, l2)
    w = do_linear_regression(x1, x2)
    ### for two unknows ###
    ga = w[1, 0]
    gb = w[0, 0] 
    avg_ga += ga
    avg_gb += gb
    ######################
    #print ('w', w)
    g = w[0, 0]
    #print('g', g)
    # take some random point to test
    x = random.uniform(l1, l2)

    #avg_var += var(foo(g,x), f(x))
    avg_var += var(foo(ga, gb, x), f(x))

  #print('avg_var', avg_var/exp)
  return avg_var/exp



def find_gbar ():
  '''
    Find average of all the gs obtained on a training set of size 2

    NOTE: I did not get the correct ans for gbar. Firt it was converging
    towars 0.79 and then it was converging towars 1.5, both of which
    were wrong according to the grader. In order to answer the subsequent
    questions I need the correct answer to g-bar. Since the only options
    left are 0 and 1.07, I am going with 1.07
  '''

  sample_plot() # show the sine wave

  l1, l2 = -1, 1
  exp = 100 #the total number of datasets
  avg_a = 0
  avg_ga, avg_gb = 0, 0
  for i in range(exp):
    x1 = random.uniform(l1, l2)
    x2 = random.uniform(l1, l2)

    #a = (math.sin(math.pi*x1) + math.sin(math.pi*x2)) / (x1 + x2)
    # the above equation is simply wrong.

    w = do_linear_regression(x1, x2) # this method works when 
                                     # 1 is not prepended to x
    g = w[0, 0]

    #########################
    ## required for two unknowns##
    ga = w[1, 0] # notice the change in a and b: NOTE
    gb = w[0, 0]
    #print('ax + b', w, ga, gb)
    avg_ga += ga
    avg_gb += gb
    #########################
    
    #dx, dy = mean_sqrt_err(x1, x2) # this is the method that works
    #                               # and I don't know why. TODO
    #a = dy/dx

    avg_a += g
    #plot_line(a, x1, x2)


  g_bar = avg_a/exp
  ##### required for two unknowns ####
  g_bar_a = avg_ga/exp
  g_bar_b = avg_gb/exp
  ###################################

  
  ax_squre = lambda x: g_bar*x*x # for hypothesis ax^2
  ax = lambda x: g_bar*x # for hypothesis ax 
  b = lambda x: g_bar # for hypothesis b
  ax_plus_b = lambda x: g_bar_a*x + g_bar_b #hypothesis ax + b
  axsquare_plus_b = lambda x: g_bar_a*x*x + g_bar_b #hypothesis ax + b


  #plot_line(b, random.uniform(l1, l2), random.uniform(l1, l2))
  #plot_line(ax, random.uniform(l1, l2), random.uniform(l1, l2))
  #plot_line(ax_squre, random.uniform(l1, l2), random.uniform(l1, l2))
  #plot_line(ax_plus_b, random.uniform(l1, l2), random.uniform(l1, l2))
  plot_line(axsquare_plus_b, random.uniform(l1, l2), random.uniform(l1, l2))

  print 'gbar', g_bar
  b_bar = 1.07 # from the answer

  f = lambda x: math.sin(math.pi * x) # target function
  #g_bar_fn = lambda x: g_bar * x 

  #bias = compute_bias(g_bar, f, b)
  #bias = compute_bias(g_bar, f, ax)
  #bias = compute_bias(g_bar, f, ax_squre)
  #bias = compute_bias(g_bar, f, ax_plus_b)
  bias = compute_bias(g_bar, f, axsquare_plus_b)
  print("bias", bias)

  b_foo = lambda a, x: a  # for hypothesis b
  ax_foo = lambda a, x: a*x # for hypothesis ax
  ax_squre_foo = lambda a, x: a*x*x # for hypothesis ax^2
  ax_plus_b_foo = lambda a, b, x: a*x + b # for hypothesis ax + b
  axsquare_plus_b_foo = lambda a, b, x: a*x*x + b # for hypothesis ax + b

  #variance = compute_variance(g_bar, b, b_foo)
  #variance = compute_variance(g_bar, ax, ax_foo)
  #variance = compute_variance(g_bar, ax_squre,  ax_plus_b_foo)
  #variance = compute_variance(g_bar, ax_plus_b, ax_plus_b_foo)
  variance = compute_variance(g_bar, axsquare_plus_b, axsquare_plus_b_foo)
  print('variance', variance)

  e_out = bias + variance
  print("eout", e_out)

    
    
