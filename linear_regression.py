from perceptron import *
#from perceptron import plearn
#from pylab import *

def prepare_Y (x, y, m, c):
  '''
    check where the points in (x,y) lies wrt  to the line
    (m, c). The points lying on:
    left is -1
    right is +1

    And returns nX1 dimensional matrix comprising of -1s and +1s
  '''

  Y = []
  for i in range(len(x)):
    if is_left([x[i], y[i]], m, c):
      Y.append(-1)
    else:
      Y.append(1)
 
  Y = matrix(Y).reshape(len(x), 1)

  return Y
      

def prepare_X (x, y):
  X = []
  d = 3 #dimensionality of the weight vector
  for i in range(len(x)):
    X.append([1, x[i], y[i]])

  X = matrix(X).reshape(len(x), d)


  return X


def calculate_e_out (g, m, c):
  E = 1000 # no of experiments to test(no of points)
  l1, l2 = -1, 1

  #getting fresh E number of points
  x = get_rand(E, l1, l2)
  y = get_rand(E, l1, l2)

  X = prepare_X(x, y)

  Y = X * g
  E_out = 0
  for i in range(E):
    if is_left([x[i], y[i]], m, c):
      if sign(Y[i]) != -1:
        E_out += 1
    else:
      if sign(Y[i]) != 1:
        E_out += 1
    
  return (E_out * 1.0) / E

  
def calculate_e_in (g, Y, X):
  ''' calculate in- sample error '''
  g_output = X * g
  E_in = 0
  for i in range(len(g_output)):
    if sign(g_output[i]) != Y[i]:
      E_in += 1

  return (E_in * 1.0)/len(Y)
  
  
def start ():
  ''' 
    Computes the X_dagger for a particulare set of points ie, X
  '''
  N = 1000 # number of points
  l1, l2 = -1, 1
  (linex, liney) = get_random_line(l1, l2)
  x = get_rand(N, l1, l2)
  y = get_rand(N, l1, l2)

  #plt.plot(x, y, 'ro')
  #plt.plot(linex, liney, 'g-', lw=2)
  #plt.axis([-1, 1, -1, 1])

  (m, c) = get_line_eq(linex, liney)
  #extend_line(m, c, l1, l2)

  Y = prepare_Y(x, y, m, c)

  X = prepare_X(x, y)
    
  X_dagger = inv(transpose(X) * X) * transpose(X)
  g = X_dagger * Y
  return (g, Y, X, m, c)


def do_perceptron_learning (g, m, c):
  ''' 
      prepare the training set and call the
      perceptron learning algorithm
  '''

  h = g
  E = 10 # no of points
  x = get_rand(E, -1, 1)
  y = get_rand(E, -1, 1)

  # preparing training set for perceptron
  training_set = []
  for i in range(len(x)):
    if is_left([x[i], y[i]], m, c):
      training_set.append(((1, x[i], y[i]), -1))
    else:
      training_set.append(((1, x[i], y[i]), 1))

  iterations = plearn(training_set, h)
  return iterations[1]
      

def run ():
  E = 100 # number of times E_in needs to be calcluated
  E_in, E_out = 0, 0
  iterations = 0
  for i in range(E):
    (g, Y, X, m, c) = start()
    E_in += calculate_e_in(g, Y, X)
    E_out += calculate_e_out(g, m, c)

    #if i > 99 and i%100 == 0:
    #  print 'E_out', 'E', E_out/i, i

    iterations += do_perceptron_learning(g, m, c)

    # plotting g alongwith f
    #extend_line(g.item(1), g.item(2), -1, 1, 'b-')
   
  E_in = (E_in * 1.0) / E
  E_out = (E_out * 1.0) / E
  iterations /= E

  print 'E_in, E_out, iterations', E_in, E_out, iterations

