'''
  Final questions 13 to 18. 
  Comparing basic rbf with svm.
  Target function: f(x) = sign( x2 - x2 + 0.25*sin(pi*x1) )
  This target function induces a small non-linearity
'''


from perceptron import *
import math

from sklearn.svm import SVC
import numpy as np

colors = ['bo', 'go', 'ro', 'co', 'mo', 'yo', 'bx', 'gx', 'rx', 'cx', 'mx', 'yx', 'kx']

def plot_points (x1, x2, y):
  '''
    Plot the points in two colors
  '''

  for i in range(len(y)):
    if y[i] == +1:
      plt.plot(x1[i], x2[i], 'rx')
    else:
      plt.plot(x1[i], x2[i], 'yo')


def plot_means (means, color='ko'):
  '''
    plot the k-means
  '''
  x, y = [], []
  for i in means:
    x.append(i[0])
    y.append(i[1])

  #print 'color', color
  #a = raw_input('enter')
  #print a
  plt.plot(x, y, color)
      


def plot_svs (P):
  '''
    P[[x1, x2][x1,x2]..]
  '''
  x, y = [], []
  #print 'len', len(P), P
  for i in range(len(P)):
    x.append(P[i][0])
    y.append(P[i][1])

  #print x, y
  plt.plot(x, y, 'ro')
    
  
def plot_clusters_and_means (clusters, means):
  '''
  '''
  for k in range(len(clusters)):
    plot_means(clusters[k], color = colors[k])

  plot_means(means, color='ko')


def get_training_set (N):
  '''
    Return the training set X = [-1,1] * [-1,1]
    Target function: f(x) = sign( x2 - x2 + 0.25*sin(pi*x1) )
    N: the number of points in the training set
  '''

  l1, l2 = -1, 1
  x1 = get_rand(N, l1, l2)
  x2 = get_rand(N, l1, l2)

  target = lambda x1, x2: sign(x2-x1 + 0.25*math.sin(math.pi*x1))
  x_train, y_train = [], []


  for i in range(N):
    x_train.append([x1[i], x2[i]])
    y_train.append(target(x1[i], x2[i]))

  #plot_points(x1, x2, y_train)

  return x_train, y_train


def get_ein_and_eout_svm_rbf (x, y, gamma):
  '''
    the kernel is rbf of the form
    exp(-gamma||x - xn||^2)
  '''
  X = np.array(x)
  Y = np.array(y)

  kernel = 'rbf'

  clf = SVC(C=1e4, kernel=kernel, gamma=gamma)
  clf.fit(X, Y)

  e_in = 0
  for i in range(len(X)):
    y_o = clf.predict(X[i])
    if y_o[0] != Y[i]:
      e_in += 1

  e_in = e_in/float(len(X))

  # calculating e_out
  e_out = 0
  l1, l2 = -1, 1
  N = 100
  x1 = get_rand(N, l1, l2)
  x2 = get_rand(N, l1, l2)
  target = lambda x1, x2: sign(x2-x1 + 0.25*math.sin(math.pi*x1))

  y_real = []
  y_obt = []
  for i in range(N):
    y_r = target(x1[i], x2[i])
    y_o = clf.predict([x1[i], x2[i]])
    if y_r != y_o:
      e_out += 1
    #print sign(y_r), y_o
    
  e_out /= float(N)
  


  #plot_svs(X[clf.n_support_])
  
  return e_in, e_out


def minimal_dist (x, means):
  '''
    return k that minimisex distance between x and means[k]
  '''

  mag = lambda x1, x2: math.sqrt(x1**2 + x2**2) #magnitude of vector

  k =  0
  min_dis = 1e6 # an arbitrary high number
  for i in range(len(means)):
    dis =  mag(x[0] - means[i][0], x[1] - means[i][1])
    if dis < min_dis:
      min_dis = dis
      k = i

  return k
    

def get_clusters (means, X):
  '''
    find the clusters among the points depending on the means
    by minimising the distance between the points and the means
  '''

  clusters = [ [] for k in range(len(means)) ]

  for i in range(len(X)):
    k = minimal_dist(X[i], means)
    clusters[k].append(X[i])

  return clusters


def get_means (clusters):
  '''
    Return the mean of each different cluster.
    They will become the new mean.
  '''

  means = []
  for k in range(len(clusters)):
    mean = [0, 0]
    #plot_means(clusters[k], color=colors[k])
    for point in clusters[k]:
      mean[0] += point[0]
      mean[1] += point[1]

    mean[0] /= float(len(clusters[k]))
    mean[1] /= float(len(clusters[k]))
    means.append(mean)

  return means


def is_equal (A, B):
  '''
    Perform elementwise comparison between vectors A and B
  '''
  for i in range(len(A)):
    if A[i] != B[i]:
      return False

  return True
      

def get_random_points (K, X):
  '''
    return K random points from X
  '''
  import random
  l = range(len(X))
  ans = []
  for i in range(K):
    random.shuffle(l)
    ans.append(X[l.pop()])

  return ans

def is_any_cluster_empty (clusters):
  '''
  '''
  for k in clusters:
    if len(k) == 0: return True

  return False

def get_mus (X, K, rand):
  '''
    Ge k means from x using Lloyd's Algorithm
    1. Fix random means.
    2. put x in cluster[i], if ||x-mean[i]|| < ||x-all_means|| 
  '''

  clusters = [ [] for k in range(K) ]

  if rand:
    #initialize means as K random points
    l1, l2 = -1, 1
    x1 = get_rand(K, l1, l2)
    x2 = get_rand(K, l1, l2)
    means = []
    for i in range(K):
      means.append([x1[i], x2[i]])

    old_means = means
  else:    
    # initialize means as K random points from X.
    old_means = get_random_points (K, X)

  #print old_means

  i = 0
  while True:
    i += 1
    clusters = get_clusters(old_means, X)
    if is_any_cluster_empty(clusters):
      return None, True

    new_means = get_means(clusters)
    #plot_means(new_means, color='ko')

    if is_equal(new_means, old_means):
      #print 'converged', i
      break

    old_means = new_means

  #plot_clusters_and_means(clusters, old_means)
  empty = is_any_cluster_empty(clusters)

  return old_means, False


def obtain_theX (x, kmeans, lam):
  '''
    obtain the phi as per L16-slide:14
  '''

  mag = lambda x1, x2: math.sqrt(x1**2 + x2**2)

  cal = lambda x, mu: math.e**(-lam*(mag(x[0] - mu[0], x[1] - mu[1])**2 ))

  # NOTE:  A bias term has to be added. 16-19. 
  # ie, an extra column of w0 with all 1s
  X = []
  for i in range(len(x)):
    row = [1] # the extra 1
    for k in range(len(kmeans)):
      row.append(cal(x[i], kmeans[k]))

    X.append(row)

  return X
      
  

def calculate_e_in (X, phi, Y, w, kmeans, lam):
  '''
    h(x) = sigma k =1 to K [wk * e **(||x-kmean||**2)]
    L16-slide:17
    E_in can be obtained in two ways.
    a) using the phi matrix(with bias)
    y = sign(phi*w)
    b) by calculating the h(x) value
    It looks like they both give the same result
  '''

  mag = lambda x1, x2: math.sqrt(x1**2 + x2**2)

  cal = lambda x, mu: math.e**(-lam*(mag(x[0] - mu[0], x[1] - mu[1]))**2 )

  y_obtained = []
  for x in X:
    y = 0
    for k in range(len(kmeans)):
      y += w[k+1]*cal([x[0], x[1]], kmeans[k]) 

    y_obtained.append(sign(y+w[0])) # w[0] is the bias

  y_obt = phi * w # method a) described above

  e_in = 0
  e_in2 = 0
  for i in range(len(y_obtained)):
    if sign(y_obt[i, 0]) != Y[i, 0]:
      e_in2 += 1

    if y_obtained[i] != Y[i, 0]:
      e_in += 1
      
  e_in /= float(len(y_obtained))
  e_in2 /= float(len(y_obt))

  #print 'ein', e_in, e_in2 #they are the same

  return e_in



def calculate_e_out (w, kmeans, lam):
  '''
    Obtain e_out using kmeans on fresh set of points
    I am using two methods that I mentioned in the 
    documentation of calculate_e_in function. It is 
    worthwhile to note that both of them provide the same output.
  '''

  N = 100 #the number of out of sample points
  l1, l2 = -1, 1
  x1 = get_rand(N, l1, l2)
  x2 = get_rand(N, l1, l2)

  target = lambda x1, x2: sign(x2-x1 + 0.25*math.sin(math.pi*x1))

  y_real = []
  for i in range(N):
    y_real.append(target(x1[i], x2[i]))


  mag = lambda x1, x2: math.sqrt(x1**2 + x2**2)

  cal = lambda x, mu: math.e**(-lam*(mag(x[0] - mu[0], x[1] - mu[1]))**2 )
  
  y_obtained = []
  for i in range(N):
    y = 0
    for k in range(len(kmeans)):
      y += w[k+1] * cal([x1[i], x2[i]], kmeans[k])

    y_obtained.append(sign(y+w[0]))

  e_out = 0

  #print len(y_obtained), len(y_real)

  for i in range(len(y_obtained)):
    if y_obtained[i] != y_real[i]:
      e_out += 1
    

  e_out /= float(len(y_obtained))

  # method 2
  x_train = []
  for i in range(N):
    x_train.append([x1[i], x2[i]])

  phi = obtain_theX(x_train, kmeans, lam)
  y_obt = phi*w
  e_out2 = 0
  for i in range(N):
    if sign(y_obt[i]) != y_real[i]:
      e_out2 += 1
    
  e_out2 /= float(N)
  #print phi


  #print 'eout', e_out, e_out2 #they are the same

  return e_out





def regular_RBF (x, y, k, gamma, rand=False):
  '''
    Do regular RBF.
    rand = True means the inital means should be randomised.
    rand = False means the initial means should be randomised from x.
  '''

  empty = True
  while empty:
    kmeans, empty = get_mus(x, k, rand)
  #print 'empty', empty

  X = obtain_theX(x, kmeans, gamma)
  X = matrix(X)
  Y = matrix(y).reshape(len(y), 1)

  #print X.shape, Y.shape

  X_dagger = inv(transpose(X) * X) * transpose(X)
  #print X_dagger.shape
  w = X_dagger * Y
  #print w

  e_in = calculate_e_in(x, X, Y, w, kmeans, gamma)

  e_out = calculate_e_out(w, kmeans, gamma)

  return e_in, e_out

  

def start (N):

  # Q13
  #gamma = 1.5
  #iterations = 100
  #count = 0
  #for i in range(iterations):
  #  x_train, y_train = get_training_set(N)
  #  e_in, e_out = get_ein_and_eout_svm_rbf(x_train, y_train, gamma)
  #  print 'e_in', e_in, e_out
  #  if e_in != 0: count += 1
  #
  #print 'non-separable %', count/float(iterations)

  # Q14
  #count = 0
  #iterations = 100
  #for i in range(iterations):
  #  x_train, y_train = get_training_set(N)
  #  k = 9
  #  gamma = 1.5
  #  e_in1, e_out1 = regular_RBF(x_train, y_train, k, gamma, rand=True)

  #  e_in2, e_out2 = get_ein_and_eout_svm_rbf(x_train, y_train, gamma)

  #  if e_out2 < e_out1: count += 1

  #print 'kernel better than regular', count/float(iterations)

  #Q15
  count = 0
  iterations = 100
  for i in range(iterations):
    x_train, y_train = get_training_set(N)
    k = 12
    gamma = 1.5
    e_in1, e_out1 = regular_RBF(x_train, y_train, k, gamma, rand=True)

    e_in2, e_out2 = get_ein_and_eout_svm_rbf(x_train, y_train, gamma)

    if e_out2 < e_out1: count += 1

  print 'kernel better than regular', count/float(iterations)


  # Q16.
  #x_train, y_train = get_training_set(N)
  #K = [9, 12]
  #for k in K:
  #  e_in, e_out = regular_RBF(x_train, y_train, k, gamma)
  #  print 'k =', k, 'e_in', e_in, 'e_out', e_out


  # Q17
  #x_train, y_train = get_training_set(N)
  #k = 9
  ##lams = [1.5, 2]
  #e_in_up, e_out_up, e_in_eq, e_out_eq = 0, 0, 0, 0
  #iterations = 100
  #for i in range(iterations):
  #  gamma = 1.5
  #  e_in, e_out = regular_RBF(x_train, y_train, k, gamma, rand=True)
  #  #print 'gamma=', gamma, 'e_in', e_in, 'e_out', e_out
  #  gamma = 2
  #  e_in1, e_out1 = regular_RBF(x_train, y_train, k, gamma, rand=True)
  #  #print 'gamma=', gamma, 'e_in', e_in1, 'e_out', e_out1
  #  if e_in1 > e_in:  e_in_up += 1
  #  if e_in1 == e_in: e_in_eq += 1
  #  if e_out1 > e_out: e_out_up += 1
  #  if e_out1 == e_out: e_out_eq += 1

  #print 'e_in_up', e_in_up, 'e_out_up', e_out_up, 'e_in_eq' ,e_in_eq, 'e_out_eq', e_out_eq

    
  # Q18.
  #k = 9
  #gamma = 1.5
  #count = 0
  #iterations = 100
  #x_train, y_train = get_training_set(N)
  #for i in range(iterations):
  #   e_in, e_out = regular_RBF(x_train, y_train, k, gamma,  rand=True)
  #   print 'e_in', e_in
  #   if e_in == 0:
  #     count += 1

  #print count/float(iterations)

  


