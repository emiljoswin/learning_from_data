'''
  Started really late into this and I am really doubtful about pulling this out.
  
  Compare PLA with SVM
'''

from perceptron import *

from cvxopt import matrix as cmatrix#for SVM
from cvxopt import solvers #for SVM

solvers.options['show_progress'] = False # to disable qp progress output

def test_plot3 (w1, w2, w3, color='g-'):
  plt.plot([-1, 1], [-w1/w3 - (w2/w3)*-1, -w1/w3 - (w2/w3)*1], color)
  draw()


def obtain_P (training_set):
  '''
    Obtain P.
    Be aware that the matrix is filled columnwise in cvxop rather 
    than rowwise as in numpy
    alpha = [[y1y1 X1tx1,  y2y1 X2tX1.., yNy1 XNtX1]..[y1yN X1tXN..yNyN XntXn]]
  '''

  cmat = [] # the cmatrix, filled by colum

  for i in range(len(training_set)): # each column of alpha
    colum = []
    for j in range(len(training_set)):
      #print('y',j,'y',i,'xt',j,'x',i) #just scanning through the index

      yj = training_set[j][1]
      yi = training_set[i][1]

      # This is the actual form of Xn which is a colum vector

      # NOTE: Initially I forgot to exclude x0, which was causing 
      # disagreeement between b values.
      Xj = transpose(matrix([training_set[j][0][1], training_set[j][0][2]])) 
      Xi = transpose(matrix([training_set[i][0][1], training_set[i][0][2]]))
      #Xj = transpose(matrix(training_set[j][0]))
      #Xi = transpose(matrix(training_set[i][0]))

      temp = (yj*yi*transpose(Xj)*Xi)[0, 0]

      #print(yj, yi, Xj, Xi, transpose(Xj)*Xi, temp)

      colum.append(temp)
    cmat.append(colum)

  cmat = cmatrix(cmat) #converting to matrix from in cvxopt
  #print cmat

  return cmat


def do_svm (training_set):
  '''
    formulate the quadratic programming problem and solve for it
    using cvxopt
  '''
  n = len(training_set) #no of training examples
  G = -1*identity(n)
  G = cmatrix(G) # converting to cvx's matrix
  #print 'G', G

  h = matrix(zeros(n)).reshape(n, 1)
  
  h = cmatrix(h)
  #print 'h', h

  b = matrix(zeros(1)) # even constants are single valued matrices
  b = cmatrix(b)
  #print 'b', b

  A = [] # this is Yt
  for data in training_set:
    A.append(data[1])

  A = matrix(A)
  A = cmatrix(A, tc='d')
  #print 'A', A, A.size

  #print(A)

  q = cmatrix(array(repeat(-1, n)), tc='d')
  #print 'q', q, q.size

  #fill alpha/P
  P = obtain_P(training_set)
  #print 'P', P, P.size

  #print 'q', q, q.size
  sol = solvers.qp(P, q, G, h, A, b)

  #print 'x', sol['x']
  return sol['x']

def calculate_w (alpha, training_set):
  '''
    return weight vector which is 
    sum (alpha[n], y[n], x[n])
    along with the indices of support vectors.
    The indices are needed to identify the support vectors and also
    the w0 or b

    IMPORTANT : Use the Support Vectors to calculate w
  '''

  w = [0.0, 0.0] # the weights

  indices = [] # this stores the indices of the support vectors

  for i in range(len(training_set)):
    x = training_set[i][0]
    y = training_set[i][1]

    if (alpha[i] < 0.0001): # for condition alpha[i] > 0
      continue

    #print alpha[i], ' is large enough to be a support vector', i

    indices += [i]

    w[0] += alpha[i] * y * x[1]
    w[1] += alpha[i] * y * x[2]

  return w, indices

def get_b (w, training_set, indices):
  '''
    yn(WtXn + b) = 1
    The correctness of the program is determined by calculating b
    and verifying that it is same for all n

    b = (1 - yn(WtXn)) / yn
  '''
  w = matrix(w) 

  for i in range(len(training_set)):
    
    if i in indices:
      x = [training_set[i][0][1], training_set[i][0][2]]
      y = training_set[i][1]
      x = matrix(x)
      b = (1 - y * (w * transpose(x))) / y
      #print 'b', b[0, 0]

  return b[0, 0] # actually it is true only when value of b is equal


def obtain_test_dataset (N, m, c):
  '''
    Generate test data set of N points and return X and Y
  '''
  l1, l2 = -1, 1
  x = get_rand(N, l1, l2)
  y = get_rand(N, l1, l2)

  (linex, liney) = get_random_line(l1, l2)

  y_original = [] # the actual y value
  X = []
  for i in range(len(x)):
    X.append([1, x[i], y[i]])
    if is_left([x[i], y[i]], m, c):
      y_original.append(-1)
    else:
      y_original.append(1)

  X = matrix(X)
  Y = matrix(y_original)
  return X, Y

def calculate_pla_E_out (h, X, Y):
  '''
    Calulate PLA Eout as disagreement between Y and X*h
  '''

  h = matrix(h)
  y = X * transpose(h) # the y value due to decision boundary

  sign = lambda x: -1 if x < 0 else 1

  y_obtained = [sign(i) for i in y]

  e_out = 0

  for i in range(len(y_obtained)):
    if y_obtained[i] != Y[0, i]:
      e_out += 1

  e_out = e_out/float(len(y_obtained))
  #print 'e_out', e_out
  return e_out


def obtain_trainingset (N):
  '''
    N is the number of training points
  '''

  l1, l2 = -1, 1
  x = get_rand(N, l1, l2)
  y = get_rand(N, l1, l2)
  (linex, liney) = get_random_line(l1, l2)

  plt.plot(x, y, 'ro') #plot
  plt.plot(linex, liney, 'g-', lw=2) #plot
  plt.axis([-1, 1, -1, 1]) #plot

  (m, c) = get_line_eq(linex, liney)

  extend_line(m, c, l1, l2) #plot

  training_set = []

  count = 0 # variable used to see if points all lie on one side of line

  for i in range(len(x)):
    if is_left([x[i], y[i]], m, c):
      training_set.append(((1, x[i], y[i]), -1))
      count += 1
    else:
      training_set.append(((1, x[i], y[i]), 1))

  if count == 0 or count == len(x):
    print 'all points on the same line'
    return None, None, None

  return training_set, m, c

def is_svm_better (training_set, m, c):
  
  h = [0.0, 0.0, 0.0]
  (a, iterations, h) = plearn(training_set, h)

  test_plot3 (h[0], h[1], h[2])

  # obtain the solution from quadratic programming
  alpha = do_svm(training_set)

  # w's size is 2. w0 is actually b
  w, indices = calculate_w(alpha, training_set) 
  print 'no of Support Vectors', len(indices)

  b = get_b(w, training_set, indices)

  #print w, b

  w = [b, w[0], w[1]] # the final w from SVM
  #print w


  # compare the out of sample error values of PLA and SVM

  N = 100 # no of test points

  test_X, test_y = obtain_test_dataset(N, m, c)

  E_out_pla = calculate_pla_E_out (h, test_X, test_y)
  #print 'eout pla', E_out_pla
  E_out_svm = calculate_pla_E_out (w, test_X, test_y)
  #print 'eout svm', E_out_svm

  if E_out_svm > E_out_pla:
    return True
  return False



def experiment (N):
  '''
     N is the number of points
  '''

  iterations = 100 # no of expermients

  # if all points lie on same line, the experiment is no longer valid
  # 'exp_count' keeps track of the no of valid experiments
  count, exp_count = 0, 0

  for i in range(iterations):
    training_set, m, c = obtain_trainingset (N)
    if training_set != None:
      exp_count += 1 # this is the count of valid experiments
      if is_svm_better(training_set, m, c):
        count += 1

      print 'ratio', count/float(exp_count)

  print count/float(exp_count)


  

  
