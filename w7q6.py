'''
  Doing a monte-carlo simulation to arrive at the result because I do not know the analytical approach.
  The analytica approach involves CDF etc. Have a look into it when you get time.
'''

#import random.uniform as uniform
import random

def start ():
  # uniform => Get a random number in the range [a, b) or [a, b] depending on rounding. 
  # I guess this is the best one that includes 1 also
  e1 = random.uniform(0, 1)
  e2 = random.uniform(0, 1)
  em = min(e1, e2)
  eM = max(e1, e2)
  return e1, e2, em, eM


def iterate (N):
  eone, etwo, emin, emax = 0.0, 0.0, 0.0, 0.0
  for i in range(N):
    e1, e2, em, eM = start()
    eone += e1
    etwo += e2
    emin += em
    emax += eM

  print eone/N, etwo/N, emin/N, emax/N
    
  
