'''
  We do linear regression on a sample with noise. The E_in is found to be around 0.5 which is 
  a lot higher. This means that the simple linear hypothesis is not able to give a good hypothesis.
  Things become interesting when we introduce non-linear transformation to the picture.
  With transformation of the form

        X = (1, x1, x2, x1*x2, x1**2, x2**2)

  We find that E_out is really small even with noise.
'''

from linear_regression import *
from perceptron import *
from random import randint, sample


def target_fn (x, y):
  return x**2 + y**2 - 0.6

def prepare_Y (x, y):
  '''
    prepare y with target function given in this module
    instead of linear_regression module
  '''
  Y = []
  for i in range(len(x)):
    Y.append(sign(target_fn(x[i], y[i])))

  Y = matrix(Y).reshape(len(x), 1)

  return Y


def random_flip_Y (Y, n):
  '''
    Flip the signs of n number of random points in Y
  '''
  #n = 5 # test
  sample_points = sample(range(len(Y)), n)
  for i in range(n):
    Y[sample_points[i]] *= -1

  return Y 


def prepare_X_nt (x, y):
  ''' 
    Applies for non-linear transformation of input ie,
    Prepares matrix X with the feature (1, x1, x2, x1x2, x1^1, x2^2k)
  '''
  X = []
  d = 6
  for i in range(len(x)):
    X.append([1, x[i], y[i], x[i]*y[i], x[i]**2, y[i]**2]) 
    #X.append([1, y[i], x[i], x[i]*y[i], y[i]**2, x[i]**2]) 

  X = matrix(X).reshape(len(x), d)

  return X


def start ():
  points = 1000
  l1, l2 = -1, 1
  (linex, liney) = get_random_line(l1, l2)
  x = get_rand(points, l1, l2)
  y = get_rand(points, l1, l2)


  (m, c) = get_line_eq(linex, liney) # this line not needed

  Y = prepare_Y(x, y) # with new target function

  #X = prepare_X(x, y)
  X = prepare_X_nt(x, y)

  noise_size = points/10

  Y = random_flip_Y(Y, noise_size)

  X_dagger = inv(transpose(X) * X) * transpose(X)
  g = X_dagger * Y
  return (g, Y, X, m, c)


def plot_both_hypothesis (g1, g2):
  '''
    I found that despite having a similary of only 50% in the
    value of hypothesis, both of them classify points with a good
    similarity of around 90%. So, I thought I would plot them 
    and see pictorially how they look and get some intuition
    regarding how they work even when half of the features's values
    disagree.
  '''
  #y = get_rand(100, -1, 1)

  #for i in range(len(y)):
  #  y1 = 

  '''
    Just realized that plotting this is not possible because,
    the hypothesis hyperplane has 6 dimensions :P
  '''

def compare_hypothesis (g):

  ''' 
    problem 9: Compare the given hypothesis's with g and
    find the one that agrees the most ie, the diff_count should be minimum 
    for that particular hypothesis
  '''

  d = 6 #the dimensions

  g = matrix(g).reshape(d, 1)

  # the hypothesis's given as the options. We must find out which hypothesis among these
  # will match the most with g
  g1 = matrix([-1, -0.05, 0.08, 0.13, 1.5, 1.5]).reshape(d, 1)
  g2 = matrix([-1, -0.05, 0.08, 0.13, 1.5, 15]).reshape(d, 1)
  g3 = matrix([-1, -0.05, 0.08, 0.13, 15, 1.5]).reshape(d, 1)
  g4 = matrix([-1, -1.5, 0.08, 0.13, 0.05, 0.05]).reshape(d, 1)
  g5 = matrix([-1, -0.05, 0.08, 1.5, 0.15, 0.15]).reshape(d, 1)

  points = 100
  x = get_rand(points, -1, 1)
  y = get_rand(points, -1, 1)

  X = prepare_X_nt(x, y) # prepare non linear transformed X

  Y = X * g

  noise_size = points/10
  Y = random_flip_Y(Y, noise_size) # adding noise 

  Y1 = X * g5 # NOTE: It is found that g1 is the one that matches the most, ie, diff_count is minimum for g1
  
  diff_count = 0
  for i in range(points):
    if sign(Y[i]) != sign(Y1[i]):
      diff_count += 1

  print 'diff', (diff_count * 1.0) / points

  
  plot_both_hypothesis(g1, g)

def calculate_e_out (g):
  '''
    calculate E_out using the the target function in this module
  '''
  d = 6 # the dimensionality of the feature vector

  # NOTE: both the below RHS values for g will work because they are found to be really close while comparison
  #       Hence, we can use whatever we want. I used the answer to the previous question (9)
  #g = matrix(g).reshape(d, 1)
  g = matrix([-1, -0.05, 0.08, 0.13, 1.5, 1.5]).reshape(d, 1) # this is the matched

  E = 1000 # no of points
  l1, l2 = -1, 1
  
  x = get_rand(E, l1, l2)
  y = get_rand(E, l1, l2)

  X = prepare_X_nt(x, y)

  Y = prepare_Y(x, y) #with target function in this module

  noise_size = E/10
  Y = random_flip_Y(Y, noise_size)

  gY = X * g
  E_out = 0
  for i in range(E):
    if sign(gY[i]) != Y[i]:
      E_out += 1

  return (E_out * 1.0) / E


def run():
  E = 100 # no of experiments
  E_in, E_out = 0, 0
  avg_g = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]

  for i in range(E):
    (g, Y, X, m, c) = start()
    E_in += calculate_e_in(g, Y, X)

    avg_g[0] += g.item(0)
    avg_g[1] += g.item(1)
    avg_g[2] += g.item(2)
    avg_g[3] += g.item(3)
    avg_g[4] += g.item(4)
    avg_g[5] += g.item(5)
    

  E_in = (E_in * 1.0) / E
  avg_g[0] /= E
  avg_g[1] /= E
  avg_g[2] /= E
  avg_g[3] /= E
  avg_g[4] /= E
  avg_g[5] /= E

  compare_hypothesis(avg_g)

  print 'E_in', E_in, avg_g

  print "E_out", calculate_e_out(avg_g)




  
