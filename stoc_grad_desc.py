'''
  HW 5, q8 - q10
  Run stochastic gradien descent.
'''

from perceptron import *
from linear_regression import *
from math import e
import math


def get_X (x, y):
  '''
    returns matrix
    Same as prepare_X, except that there is no 1 prepended
  '''
  X = []
  d = 3 # dimensionality
  for i in range(len(x)):
    X.append([1, x[i], y[i]])

  X = matrix(X).reshape(len(x), d)

  return X

def do_sgd (X, Y):
  '''
    Stochastic Gradient Descent.
    W(0) is all zeros
    eta = 0.01 (learning rate)
  '''
  w = matrix([[1], [0], [0]])
  eta = 0.01 # learning rate given in the question
  N = range(len(X))
  shuffle(N)
  

  k = 0 #k indicates the total number of epochs
  while True:
    k += 1
    N = range(len(X))
    shuffle(N)
    wpre = w
    
    #one full epoch ie, exhaustion of all randomly permuted examples
    while len(N) != 0:
      n = N.pop()
      num = Y[n, 0] * X[n] # Y[n, 0] 0 to get the actual value from matrix
      den = 1 + e**(Y[n, 0] * X[n] * w)[0, 0]
      w = w - eta * matrix(-1*num/den).reshape(len(w), 1) # I missed the -1 before

    # End of one epoch
   
    w_diff = wpre - w

    my_norm = lambda v: sqrt(sum(i**2 for i in v))[0, 0]

    #print 'mynorm', k,  my_norm(w_diff) 
    if my_norm(w_diff) < 0.01:
      print 'breaking at', my_norm(w_diff), 'epochs=', k, 'w=', w
      return w
      #break


def calculate_e_out (w, points, m, c):
  '''
    calculate E_out on points using w
  '''
  l1, l2 = -1, 1
  N = points
  x = get_rand(N, l1, l2)
  y = get_rand(N, l1, l2)

  X = get_X(x, y)
  Y = prepare_Y(x, y, m, c)

  #print w, X
  Y_obtained = X*w

  #print Y_obtained, Y

  error = 0
  for i in range(len(X)):
    xw = X[i]*w
    exp = (-Y_obtained[i, 0] * xw)[0, 0]
    error += math.log(1 + e**exp) #the cross entropy error lec9, slide 16

  print('e_out', error/float(len(X)))



  

def start ():
  N = 100 # no of points
  l1, l2 = -1, 1
  x = get_rand(N, l1, l2)
  y = get_rand(N, l1, l2)

  (linex, liney) = get_random_line(l1, l2)
  (m, c) = get_line_eq(linex, liney)

  X = get_X(x, y)
  Y = prepare_Y(x, y, m, c)

  w = do_sgd(X, Y)

  points = 100 #given in question
  e_out = calculate_e_out(w, points, m, c)
  print 'learned weight', w
      

